
// This file doesn't do anything, it is just to test javascript code. The functioning code is in __init__.py.
const minsize;
const maxsize;
const fontlist = [];

function getSize(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max-min) + min);
};

function getFont(fonts){
    return Math.floor(Math.random() * fonts.length);
};

document.getElementById("text").style.fontSize = "''' + random_size(config['minfontsize'], config['maxfontsize']) + '''""px";
document.getElementById("text").style.marginTop = getSize(0,150) + "px";
document.getElementById("text").style.fontStyle = "''' + random.choice(styles) + '''";
document.getElementById("text").style.fontFamily = "''' + random.choice(fonts) + '''";
document.querySelectorAll('b').forEach(el => {
    el.style.color = "''' + random.choice(bold_text_colors) + '''";
});
document.querySelectorAll('u').forEach(el => {
    el.style.color = "''' + random.choice(underline_text_colors) + '''";
});
document.querySelectorAll('i').forEach(el => {
    el.style.color = "''' + random.choice(underline_text_colors) + '''";
});
