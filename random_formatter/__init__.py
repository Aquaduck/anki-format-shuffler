import random
import json
import math
from aqt import gui_hooks, mw


# Open config
config = mw.addonManager.getConfig(__name__)


current_theme = None

# For debugging JS code, not used in addon
#with open('random_formatter/config.json', mode="r", encoding="utf-8") as read_file:
#   config = json.load(read_file)


def random_size(min, max):
    min = math.ceil(min)
    max = math.floor(max);
    return random.randrange(min, max, 1)


def javascript_builder():
    # Define dictionary with every JS snippet per feature
    feature_javascript = {
        "random-fonts": "document.getElementById('text').style.fontFamily = \"" + str(random.choice(config['fonts'])) + "\";",
        "random-font-sizes": "document.getElementById('text').style.fontSize = \"" + str(random_size(config['minfontsize'], config['maxfontsize'])) + "px\";",
        "random-font-styles": "document.getElementById('text').style.fontStyle = \"" + str(random.choice(config['styles'])) + "\";",
        "random-margins": "document.getElementById('text').style.marginTop = \"" + str(random_size(config['minfontsize'], config['maxfontsize'])) + "px\";",
        "random-text-align": "document.getElementById('text').style.textAlign = \"" + str(random.choice(config['textalign'])) + "\";",
        "random-bold-text-colors": "document.querySelectorAll('b').forEach(el => {el.style.color = \"" + str(random.choice(config['bold-text-colors'])) + "\"});",
        "random-underline-text-colors": "document.querySelectorAll('u').forEach(el => {el.style.color = \"" + str(random.choice(config['underline-text-colors'])) + "\"});",
        "random-italic-text-colors": "document.querySelectorAll('i').forEach(el => {el.style.color = \"" + str(random.choice(config['italic-text-colors'])) + "\"});"
    }

    # Construct JS script to be inserted before every card
    string = ""

    for key, value in config['enabled-features'].items():
        if value:
            string += feature_javascript[str(key)] + "\n"
    return "<script>\n" + string + "</script>"


# Insert JS script to be appended before card
def randomize(html, card, context):
    global current_theme
    if context == "reviewQuestion" or context == "previewQuestion":
        current_theme = javascript_builder()
        return current_theme + html
    else:
        return current_theme + html

# For debugging JS code, not used in addon
def test():
    return javascript_builder()

# Use anki's hook to append randomize function to every card prior to its review/preview
gui_hooks.card_will_show.append(randomize)

# For debugging JS code, not used in addon
#print(test())
#print(test())
